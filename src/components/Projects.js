import React, { useEffect, useState,useContext } from "react";
import '../css/avatar.css';
import imgcosmetica from '../img/pro3.png';
import agenda from '../img/pro2.png';
import wordpress from '../img/pro.png';
import facebook from '../img/facebook.jpg'
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';
import { AiFillGithub,AiFillPlayCircle,AiOutlineEye} from "react-icons/ai";
import mainfacebook from '../img/mainfacebook.jpg'
import facebookvideo from '../img/facebookvideo.mp4'
import filmmain from '../img/mainfilm.jpg'
import film from '../img/film.jpg'
import filmvideo from '../img/filmvideo.mp4'
import whafilm from '../img/whafilm.mp4'
import whamain from '../img/whamain.jpg'
import AppContext from "./context";
import { modalInfo } from "./data";




export default function Projects() {
  const [selectedProject,setSelectedProject]=useState(modalInfo.movie)
  const [open, setOpen] = React.useState(false);
  const [openWebModal, setOpenWebModal] = React.useState(false);

  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false); 
  
  const handleOpenWeb = () => setOpenWebModal(true);
  const handleCloseWeb = () => setOpenWebModal(false);


  const context = useContext(AppContext);

  useEffect(()=>{
    console.log(context.language);
    // context.changeLanguage("wtfff");
  },[])
  const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 650,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
  };
  const webstyle = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 686,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
  };
  function ChildModal(props) {
    const [open, setOpen] = React.useState(false);
    const handleOpen = () => {
      setOpen(true);
    };
    const handleClose = () => {
      setOpen(false);
    };


    return (
      <React.Fragment>
        {/* <Button onClick={handleOpen}>Open Child Modal</Button> */}
        <div id="cursor" onClick={handleOpen}>
        <AiFillPlayCircle size={40}></AiFillPlayCircle>
        <b id="title">Demo</b>
        </div>  
        <Modal
          hideBackdrop
          open={open}
          onClose={handleClose}
          aria-labelledby="child-modal-title"
          aria-describedby="child-modal-description"
        >
          <Box sx={{ ...style, height:800, width: 650 }}>
            {/* <h2 id="child-modal-title">Text in a child modal</h2>
            <p id="child-modal-description">
              Lorem ipsum, dolor sit amet consectetur adipisicing elit.
            </p> */}
            <video id="child-modal-description" width="400" height="700" controls>
            <source src={props.video} type="video/mp4"></source>
            </video>
            <br></br>
            <Button style={{position:"absolute",bottom:"24px",left:"150px"}} onClick={handleClose}>Close demo</Button>
          </Box>
        </Modal>
      </React.Fragment>
    );
  }
  const WebModal=()=>{
    return(
      <Modal
      open={openWebModal}
      onClose={handleCloseWeb}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description"
    >
      <Box sx={webstyle}>
      <Typography align="center" id="modal-modal-title" variant="h6" component="h2" sx={{ mt: 0.1, }}>
             {selectedProject.title}
        </Typography>
        <img src={selectedProject.modal_photo} id="modal_image" height={350} width={620}></img>
        <br></br>
        <br></br>
        <b>{selectedProject.title_description}</b> 
            <br></br>
          {selectedProject.description}
          <br></br>
          <br></br>
           <b> {selectedProject.title_tegnologies}</b>
           <br></br>
               {selectedProject.tegnologies_description}
           <br></br> 
           <br></br> 
           <div align="center">
           <a href={selectedProject.modal_git} target="open"><AiFillGithub size={40}></AiFillGithub>
           <b id="title">Repository</b>
           </a>
           <a id="espacio" href={selectedProject.modal_url} target="open"><AiOutlineEye size={40}></AiOutlineEye>
           <b id="title"> View Web</b>
           </a>
           </div>


      </Box>
    </Modal>
    )
  }
  const MyModal=(proj)=>{    
      return(
        <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
        <Typography id="modal-modal-title" variant="h6" component="h2" sx={{ mt: 1 }}>
               {selectedProject.title}
          </Typography>
          <img src={selectedProject.modal_photo} id="modal_image" height={650}></img>
          <div  id="text_modal">
            <b>{selectedProject.title_description}</b> 
            <br></br>
          {selectedProject.description}
          <br></br>
          <br></br>
           <b> {selectedProject.title_tegnologies}</b>
           <br></br>
               {selectedProject.tegnologies_description}
           <br></br>   
           <br></br>  
           <div id="cursor">
           <a href={selectedProject.modal_git}><AiFillGithub size={40}></AiFillGithub>
           <b id="title">Repository</b>
           </a>
           </div>
           <br></br>  
           
           {/* <a href="https://gitlab.com/"><AiFillPlayCircle size={40}></AiFillPlayCircle>
           <b align="center">Repository</b>
           </a>   */}

           <ChildModal video={selectedProject.modal_video}/>
          </div>
          {/* <Typography id="modal-modal-description" sx={{ mt: 1 }}>
            this app has animations,scroll,a user list and a 

          </Typography> */}
        </Box>
      </Modal>
      )
    
  }
  return (
    <div class="main">
      <MyModal></MyModal>
      <WebModal></WebModal>
    <section id="portfolio">
    <div class="container">
      <div class="portfolio-nav">
        <ul class="txt-btn">
        <h3>Projects</h3>

        </ul>
      </div>
      <div class="portfolio-content">
        <div class="box">
          <a >
            <img src={imgcosmetica} width={12} height={330} alt="" onClick={() => { setSelectedProject(modalInfo.cosmetica);handleOpenWeb()}}></img>
            Web with php,mysql,Paypal pay
          </a>
        </div>
        <div class="box">
          <a>
            <img src={agenda} height={330} onClick={() => { setSelectedProject(modalInfo.retro);handleOpenWeb()}}></img>
            Web with php, mysql database "Demo"
          </a>
        </div>
        <div class="box">
          <a >
            <img src={require('../img/deltamain.png').default} onClick={() => { setSelectedProject(modalInfo.delta);handleOpen()}} ></img>
            This is my most elaborate App
          </a>
        </div>
        <div class="box">
          <a>
            <img src={whamain} onClick={() => { setSelectedProject(modalInfo.whatsapp);handleOpen()}}></img>
            Fake WhatsApp
          </a>
        </div>
        <div class="box">
          <a>
          <img src={filmmain} onClick={() => { setSelectedProject(modalInfo.movie);handleOpen()}}></img>
          List of films with animations
          </a>
        </div>
        <div class="box">
          <a>
            <img src={mainfacebook} onClick={()=>{setSelectedProject(modalInfo.facebook);handleOpen()}}></img>
            Fake facebook with animations

          </a>

        </div>
      </div>
    </div>
  </section>

            </div>
  )
}
