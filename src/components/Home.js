import React from "react";
import '../css/avatar.css';
import Contact from "./Contact";
export default class Home extends React.Component{
    constructor(props){
        super(props);

    }
    contact=()=>{
        return <Contact></Contact>;
    }
    render(){
        return(
            <div>

<header>


    <div id="showcase-home">
      <div class="showcase-banner">
        <h1 class="subpage">{this.props.title}</h1>
        <p class="large">{this.props.description} </p>
      </div>
    </div>

  </header>                
            </div>

        )
    }
}
