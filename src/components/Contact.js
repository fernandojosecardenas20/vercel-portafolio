import React from "react";
import '../css/avatar.css';
import { BsLinkedin } from "react-icons/bs";
import { BsFileEarmarkMedical } from "react-icons/bs";
import { BsMailbox } from "react-icons/bs";
import AppContext from "./context";
import mypdf from  "./a.pdf"


export default class Contact extends React.Component{
  static contextType = AppContext

    constructor(props){
        super(props);

    }

    _initProfile(lang) {
     const context = this.context;
      //Let's fill the context with some value! You can get it from db too.
        context.changeLanguage(lang)
     //  console.log(lang);
    }
    contact=(la)=>{
        console.log("SAdfas");
    }
    render(){
        return(
            <div>
              <br></br>
    <div align="center">

        <h2>CONTACT ME</h2>
    </div>


  <section id="contact">
    <div class="box-contact-features">
    <a href={mypdf}  download> <i class="fas fa-map-marker-alt fa-2x"><BsFileEarmarkMedical size={32}/></i>
      <h3>CV</h3>
      <p>{this.props.address}</p>
      </a>

    </div>
    <div class="box-contact-features">
     <a href="https://www.linkedin.com/in/fernando-c%C3%A1rdenas-b0064718b"> <i class="fas fa-mobile-alt fa-2x"><BsLinkedin size={32}/></i> 
      <h3>Linkedin</h3>
      </a>
      <p>{this.props.phone}</p>
    </div>
    <div class="box-contact-features">
      <i class="fas fa-envelope fa-2x"><BsMailbox size={32}/></i>  
      <h3>Email</h3>
      <p>{this.props.mail}</p>
    </div>
    {/* <button onClick={()=>this._initProfile("white")}>White</button>
    <button onClick={()=>this._initProfile("dark")}>Dark</button> */}

  </section>
             

            </div>

        )
    }
}
