 import facebook from '../img/facebook.jpg'
 import mainfacebook from '../img/mainfacebook.jpg'
 import facebookvideo from '../img/facebookvideo.mp4'
 import filmmain from '../img/mainfilm.jpg'
 import film from '../img/film.jpg'
 import filmvideo from '../img/filmvideo.mp4'
 import whafilm from '../img/whafilm.mp4'
 import whamodal from '../img/whamodal.jpg'
 import deltamodal from '../img/deltaModal.jpg'
 import deltavideo from '../img/deltavideo.webm'
 import retromodal from '../img/retromodal.png'
 import modalcosmetica from '../img/modalcosmetica.png'



 const data={
    photo:"https://t3.ftcdn.net/jpg/03/91/19/22/360_F_391192211_2w5pQpFV1aozYQhcIw3FqA35vuTxJKrB.jpg",
    description:"Welcome to my portfolio I'm mobile and web developer. I will show you what I do and how I could help you in your projects",
    studies:["develeper web,","high shool"],
    abilities:["HTML","CSS","JAVASCRIPT","PHP","JAVA","REACT JS","REACT NATIVE","GIT"],
    title:"Developer",
    personalInformation:["More information about me","Download My personal CV","fernandojosecardenas20@gmail.com"]
}

export const alltext={
    ingles:{
        title:"Fake Facebook Build with React Native Expo",
        title_description:"Fake Facebook",
        description:"this app is a clone from facebook made with react native expo,only worked on home screen",
        title_tegnologies:"Technologies used",
        tegnologies_description:"React Native Animated, styled component, react native navigation"

    },
    español:{
        title:"Fake Facebook hecho con React Native Expo",
        title_description:"Fake Facebook",
        description:"Esta app es un clone de facebook hecho con react native expo,solo la pantalla de inicio",
        title_tegnologies:"Technologies used",
        tegnologies_description:"React Native Animated, styled component, react native navigation"
    
    },
    euskera:{
        title:"Facebook React Native Expo-rekin egina",
        title_description:"Fake Facebook",
        description:"Aplikazio hau facebook-en klona bat da react native expo-rekin egina, hasierako pantaila soilik",
        title_tegnologies:"Erabilitako teknologiak",
        tegnologies_description:"React Native Animated, styled component, react native navigation"
    
    }
}

export const modalInfo=
{
    facebook:{
        title:"Fake Facebook Build with React Native Expo",
        title_description:"Fake Facebook",
        description:"this app is a clone from facebook made with react native expo,only worked on home screen",
        title_tegnologies:"Technologies used",
        tegnologies_description:"React Native Animated, styled component, react native navigation",
        modal_photo:facebook,
        modal_video:facebookvideo,
        modal_git:"https://github.com/fernando19jk/fake-facebook-clone"

    },
    movie:{
        title:"Movies list made with React Native Expo",
        title_description:"Movie List with animations",
        description:"this app is a project to get data from TMDB an movie API ",
        title_tegnologies:"Technologies used",
        tegnologies_description:"React Native Animated, styled component, Expo-font, Expo-linear-gradient",
        modal_photo:film,
        modal_video:filmvideo,
        modal_git:"https://github.com/fernando19jk/list-of-movies"
    
    },
    whatsapp:{
        title:"WhatsApp clone made with React-native Cli",
        title_description:"Fake WhatsApp",
        description:"This app is a clone from WhatsApp made with react native cli,I get user data from an api and this app has 3 screens,userList,StatesList and CallList",
        title_tegnologies:"Technologies used",
        tegnologies_description:"Vector-icons, Scrollable tab view and Axios",
        modal_photo:whamodal,
        modal_video:whafilm,
        modal_git:"https://github.com/fernando19jk/fake-whatsApp-clone"

    
    },
    delta:{
        title:"App made with React-native Cli",
        title_description:"Delta Devs",
        description:"This app is the most elaborate app I have ever made and it has many technologies and features implemented: A Real time Chat,Current location map,Multi-Languages,Dark-Mode,Real time events,Login with Google, Qr system,and QR-scanner",
        title_tegnologies:"Technologies used",
        tegnologies_description:"Context: For languages and dark-mode, sockets.io for chat and real time events, React-Native-Google-Login Library, Geolocation and Map Library, Axios etc...",
        modal_photo:deltamodal,
        modal_video:deltavideo,
        modal_git:"https://gitlab.com/"
    },
    retro:{
        title:"Web made with Php and mysql DB",
        title_description:"Retro schedule",
        description:"This web has login to save personal schedul,you can upload a image,video,etc.. ",
        title_tegnologies:"Technologies used",
        tegnologies_description:"Php,Mysql DataBase,Javascript,Html,Css",
        modal_photo:retromodal,
        modal_video:deltavideo,
        modal_git:"https://gitlab.com/",
        modal_url:"https://fernandoproyect.000webhostapp.com/index.php"
    },
    cosmetica:{
        title:"Web made with Php and mysql DB",
        title_description:"E-commerce web",
        description:"This web has paypal pay,products list, user login, users crud, products crud, multi-language, shopping cart",
        title_tegnologies:"Technologies used", 
        tegnologies_description:"Php,Mysql DataBase,Javascript,Html,Css",
        modal_photo:modalcosmetica,
        modal_video:deltavideo,
        modal_git:"https://gitlab.com/",
        modal_url:"https://2021e3p1talde3.aegcloud.pro/"
    }
}

export default data;
