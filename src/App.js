import React from 'react';
import './App.css';
import About from './components/About';
import Avatar from './components/avatar';
import Contact from './components/Contact';
import data from './components/data';
import Abilities from './components/Abilities';
import Home from './components/Home';
import AppContext from './components/context';
import '../src/App.css';
import Projects from './components/Projects';






class App extends React.Component {


  constructor(props){
    super();
    this.state={
      avatar:data.photo,
      description:data.description,
      title:data.title,
     abilities:data.abilities,
     personalInformation:data.personalInformation,
     darkmode:"white",
     language:"en",
     changeLanguage:this.changeLanguage
    }
  }

  changeLanguage=(lang)=>{
    //this.setState({language:lang})
    console.log(lang);
    if(lang=="white"){
      this.setState({language:"mainbodywhite"})
    }
    if(lang=="dark"){
      this.setState({language:"mainbodydark"})
    }
  }
  render(){
    // const changeLanguage=(lang)=>{
    //   //this.setState({language:lang})
    //   console.log(lang);
    // }
    return( 
    <div class={this.state.language}>
      <AppContext.Provider value={this.state}>
      <Home title={this.state.title} description={this.state.description}></Home>
      <About></About>
      <Abilities html={this.state.abilities[0]} css={this.state.abilities[1]} javascript={this.state.abilities[2]} java={this.state.abilities[3]} php={this.state.abilities[4]} react={this.state.abilities[5]} reactnative={this.state.abilities[6]} git={this.state.abilities[7]}></Abilities>
      <Projects></Projects>
      <Contact phone={this.state.personalInformation[0]} mail={this.state.personalInformation[2]} address={this.state.personalInformation[1]}></Contact>
      </AppContext.Provider>

    </div>
    )
  }
}
export default App;
